import { Controller, Get, Param, Post, Query, Body, Put, Delete, HttpCode, HttpStatus, Res } from '@nestjs/common';
import { Response } from 'express';

@Controller('products')
export class ProductsController {


    /** Rutas no dinámicas de primeras */
    @Get('/filter')
    getProductFilter(){
        return {message:`Soy un filtro`}
    }
    /** ./Rutas no dinámicas de primeras */

    /** Endpoint con Parametros */
    @Get('/:productId')
    getProduct(@Param() product:any){
        return {message:`Product ${product.productId}`}
    }

    @Get('/:prodId')
    @HttpCode(HttpStatus.ACCEPTED)
    getProductId(@Res() response:Response, @Param('prodId') prodId:string){
        // Estilo Express
        response.status(200).send({
            message:`Product ${prodId}`
        })
        //return {message:`Product ${prodId}`}
    }
    /** ./Endpoint con Parametros */

    /** Endpoint con Parametros tipo Query*/

    @Get('')
    /*getProducts(@Query() params: any){
        const { limit, offset } = params;*/
    getProducts(
                @Query('limit') limit: number,
                @Query('offset') offset=0,
                @Query('brand') brand: string,
                )
    {
        //const { limit, offset } = params; // destructurando
        return {message:`products limit=${limit} & offset=${offset} & brand= ${brand}`};
    }
    /** ./Endpoint con Parametros tipo Query*/

    @Post()
    create(@Body() payload:any){
        return{
            message:'accion de crear',
            payload,
        }
    }

    @Put(':id')
    update(@Param('id') id:number, @Body() payload:any){
        return{
            id,
            payload,
        }
    }

    @Delete(':id')
    delete(@Param('id') id:number){
        return{
            id
        }
    }
}
