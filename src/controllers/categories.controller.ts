import { Controller, Get, Param, Post, Body } from '@nestjs/common';

@Controller('categories')
export class CategoriesController {

    @Get('/:categoryId/products/:productId')
    getCategory(
                @Param('productId') productId:string,
                @Param('categoryId') categoryId:string
                )
    {
        return {message:`Product ${productId} and ${categoryId}`}
    }
    
    @Post()
    create(@Body() payload:any){
        return{
            message: "Categories Post",
            payload,
        }
    }
}
