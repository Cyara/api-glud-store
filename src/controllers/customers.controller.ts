import { Controller, Post, Body } from '@nestjs/common';

@Controller('customers')
export class CustomersController {

    @Post()
    create(@Body() payload:any){
        return{
            message: "customers Post",
            payload,
        }
    }
}
