import { Controller, Get, Param, Query } from '@nestjs/common';
import { PassThrough } from 'stream';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    //return this.appService.getHello();
    return 'Hola'
  }

  @Get('nuevo')
  newEndpoint(){
    return 'Yo soy nuevo'
  }

  @Get('/ruta/')
  newEndpoint2(){
    return 'Yo soy nuevo2'
  }

}
